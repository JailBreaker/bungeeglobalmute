package eu.jailbreaker.globalmute;

import com.google.common.collect.Lists;
import net.md_5.bungee.api.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class Globalmute extends Plugin {

    public static final List<@NotNull String> SERVERS = Lists.newArrayList();

    @Override
    public void onEnable() {
        Messages.load(this);

        this.getProxy().getPluginManager().registerListener(this, new ChatListener());
        this.getProxy().getPluginManager().registerCommand(this, new GlobalmuteCommand());
    }

    @Override
    public void onDisable() {

    }
}

package eu.jailbreaker.globalmute;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.Cleanup;
import lombok.SneakyThrows;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

import java.io.FileReader;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;

public class Messages {

    private static final Gson GSON = new Gson();

    private static JsonObject object;

    @SneakyThrows
    public static void load(@NotNull Plugin plugin) {
        plugin.getDataFolder().mkdirs();

        Path path = Paths.get(plugin.getDataFolder().toString(), "messages.json");

        if (Files.notExists(path)) {
            @Cleanup
            InputStream in = plugin.getClass().getClassLoader().getResourceAsStream("messages.json");

            if (in == null) return;

            Files.copy(in, path);
        }

        @Cleanup
        FileReader reader = new FileReader(path.toFile());

        object = GSON.fromJson(reader, JsonObject.class);
    }

    public static void send(@NotNull CommandSender sender, @NotNull String path, Object... replacement) {
        if (!object.has(path)) {
            sender.sendMessage(TextComponent.fromLegacyText("§cDieser Pfad existiert nicht §8(§4" + path + "§8)"));
            return;
        }

        sender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', MessageFormat.format(object.get(path).getAsString(), replacement))));
    }

}

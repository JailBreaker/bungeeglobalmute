package eu.jailbreaker.globalmute;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import org.jetbrains.annotations.NotNull;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(@NotNull ChatEvent event) {
        ProxiedPlayer player = (ProxiedPlayer) event.getSender();

        if (Globalmute.SERVERS.contains(player.getServer().getInfo().getName())) {
            if (player.hasPermission("eu.jailbreaker.globalmute.bypass")) return;

            event.setCancelled(true);
            event.setMessage(null);

            Messages.send(player, "globalmute-activated-server");
        }
    }

}

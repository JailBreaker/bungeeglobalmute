package eu.jailbreaker.globalmute;

import com.google.common.base.Joiner;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.jetbrains.annotations.NotNull;

public class GlobalmuteCommand extends Command {

    public GlobalmuteCommand() {
        super("globalmute", "eu.jailbreaker.globalmute", "gmute");
    }

    @Override
    public void execute(@NotNull CommandSender sender, @NotNull String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            Messages.send(sender, "must-be-player");
            return;
        }

        if (!sender.hasPermission(this.getPermission())) {
            Messages.send(sender, "no-permission");
            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) sender;

        if (args.length != 1) {
            Messages.send(player, "globalmute-command-help");
            return;
        }

        if (args[0].equalsIgnoreCase("list")) {
            if (Globalmute.SERVERS.isEmpty()) {
                Messages.send(player, "globalmute-list-empty");
                return;
            }

            Messages.send(player, "globalmute-list", Joiner.on("§7, ").join(Globalmute.SERVERS));
        } else {
            if (!ProxyServer.getInstance().getServers().containsKey(args[0])) {
                Messages.send(player, "server-does-not-exist", args[0]);
                return;
            }

            if (Globalmute.SERVERS.contains(args[0])) {
                Globalmute.SERVERS.remove(args[0]);
                Messages.send(player, "globalmute-deactivated", args[0]);
            } else {
                Globalmute.SERVERS.add(args[0]);
                Messages.send(player, "globalmute-activated", args[0]);
            }
        }
    }
}
